package lotnisko;
import java.util.ArrayList;
import java.util.List;


public class Samolot {
	private Integer numerLotu;
	private String liniaLotu;
	private String nazwaLotu;
	private List <Pasazer> pasazerowie = new ArrayList<>();
	public Integer getNumerLotu() {
		return numerLotu;
	}
	public void setNumerLotu(Integer numerLotu) {
		this.numerLotu = numerLotu;
	}
	public String getLiniaLotu() {
		return liniaLotu;
	}
	public void setLiniaLotu(String liniaLotu) {
		this.liniaLotu = liniaLotu;
	}
	public String getNazwaLotu() {
		return nazwaLotu;
	}
	public void setNazwaLotu(String nazwaLotu) {
		this.nazwaLotu = nazwaLotu;
	}
	public List<Pasazer> getPasazerowie() {
		return pasazerowie;
	}
	public void setPasazerowie(List<Pasazer> pasazerowie) {
		this.pasazerowie = pasazerowie;
	}
	
}
