package lotnisko;
import java.util.ArrayList;
import java.util.List;


public class Pasazer {
	private String imie;
	private String nazwisko;
	private List<Bagaz> bagaze = new ArrayList<Bagaz>();
	public Pasazer (String imie, String nazwisko){
		this.imie = imie;
		this.nazwisko = nazwisko;
		
	}
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public List<Bagaz> getBagaze() {
		return bagaze;
	}
	public void setBagaze(List<Bagaz> bagaze) {
		this.bagaze = bagaze;
	} 
}
