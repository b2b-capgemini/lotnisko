package lotnisko;

public class Bagaz {
	private double dlugosc;
	private double szerokosc;
	private boolean czyPodreczny;
		
	public double getDlugosc() {
		return dlugosc;
	}
	public void setDlugosc(double dlugosc) {
		this.dlugosc = dlugosc;
	}
	public double getSzerokosc() {
		return szerokosc;
	}
	public void setSzerokosc(double szerokosc) {
		this.szerokosc = szerokosc;
	}
	public boolean isCzyPodreczny() {
		return czyPodreczny;
	}
	public void setCzyPodreczny(boolean czyPodreczny) {
		this.czyPodreczny = czyPodreczny;
	}
	
}
