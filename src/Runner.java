
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import lotnisko.Bagaz;
import lotnisko.Pasazer;
import lotnisko.Samolot;

public class Runner {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		List<Pasazer> pasazerowie = new ArrayList<>();
		boolean czyKoniec = false;
		do {
			Pasazer pasazer = stworzPasazera(scanner);

			List<Bagaz> bagaze = new ArrayList<>();
			boolean czyKoniecBagazy = false;
			do {

				Bagaz bagaz = uzupelnijDaneBagazu(scanner);
				System.out
						.println("Czy chcesz zakonczyc uzupelnianie bagazy pasazera[true, false] "
								+ pasazer.getNazwisko() + "?");
				czyKoniecBagazy = scanner.nextBoolean();
				bagaze.add(bagaz);
			} while (!czyKoniecBagazy);
			uzupelnijDanePasazera(bagaze, pasazer);
			pasazerowie.add(pasazer);
			System.out.println("Czy zakonczyc uzupelnianie danych pasazerow?");
			czyKoniec = scanner.nextBoolean();
		} while (!czyKoniec);

		Samolot samolot = uzupelnijDaneSamolotu(pasazerowie, scanner);
		daneSamolotu(samolot);

	}

	private static Bagaz uzupelnijDaneBagazu(Scanner scanner) {

		Bagaz bagaz = new Bagaz();
		System.out.println("Podaj dlugosc bagazu");
		double dlugosc = scanner.nextDouble();
		System.out.println("Podaj szerokosc bagazu");
		double szerokosc = scanner.nextDouble();
		System.out.println("Czy bagaz jest podreczny");
		boolean czyPodreczny = scanner.nextBoolean();
		bagaz.setDlugosc(dlugosc);
		bagaz.setSzerokosc(szerokosc);
		bagaz.setCzyPodreczny(czyPodreczny);
		return bagaz;
	}

	private static void uzupelnijDanePasazera(List<Bagaz> bagaze,
			Pasazer pasazer) {
		pasazer.getBagaze().addAll(bagaze);
	}

	private static Samolot uzupelnijDaneSamolotu(List<Pasazer> pasazer,
			Scanner scanner) {
		Samolot samolot = new Samolot();
		System.out.println("Podaj linie lotu");
		String liniaLotu = scanner.next();
		System.out.println("Podaj nazwe lotu");
		String nazwaLotu = scanner.next();
		System.out.println("Podaj nr lotu");
		Integer numerLotu = scanner.nextInt();
		samolot.setLiniaLotu(liniaLotu);
		samolot.setNazwaLotu(nazwaLotu);
		samolot.setNumerLotu(numerLotu);
		samolot.getPasazerowie().addAll(pasazer);
		return samolot;
	}

	private static void daneSamolotu(Samolot samolot) {
		System.out.println("Wypisujemy dane samolotu" + samolot.getNazwaLotu());
		for (Pasazer pasazer : samolot.getPasazerowie()) {
			System.out.println(pasazer.getImie());
			System.out.println(pasazer.getNazwisko());
			for (Bagaz bagaze : pasazer.getBagaze()) {
				System.out.println(bagaze.getDlugosc());
				System.out.println(bagaze.getSzerokosc());
				System.out.println(bagaze.isCzyPodreczny());
			}
		}

	}

	private static Pasazer stworzPasazera(Scanner scanner) {
		System.out.println("Podaj imie pasazera");
		String imie = scanner.next();
		System.out.println("Podaj nazwisko pasazera");
		String nazwisko = scanner.next();

		Pasazer pasazer = new Pasazer(imie, nazwisko);
		return pasazer;
	}
}
